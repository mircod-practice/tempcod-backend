# Temperature patch

Temperature patch - проект с летней практики.

## Технологии
Проект создан с помощью технологий:
* Python 3.8
* Django 4.0
* Django rest framework 3.13
	
## Установка и запуск проекта

- `python3 -m venv venv` - создать виртуальное окружение
- `source venv/bin/activate` - войти в виртуальное окружение
- `pip install -r requirements.txt` - установить зависимости
- `pre-commit install` - установка pre-commit хуков для запуска линтеров перед коммитом
- `docker-compose up -d` - поднять базу данных PostgreSQL (если Вы не используете Docker, установите PostgreSQL 
с официального сайта)
- `python src/manage.py migrate` - применить миграции к базе данных
- `python src/manage.py runserver` - запуск сервера для разработки
