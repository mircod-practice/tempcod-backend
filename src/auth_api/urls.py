from django.urls import path

from auth_api.views import (
    MyObtainTokenPairView,
    DecoratedTokenRefreshView,
    DecoratedTokenVerifyView,
    RegistrationAPIView,
    LogoutAPIView,
)

urlpatterns = [
    path("signup/", RegistrationAPIView.as_view(), name="registration"),
    path("signin/", MyObtainTokenPairView.as_view(), name="token_obtain_pair"),
    path("logout/", LogoutAPIView.as_view(), name="logout"),
    path("token/refresh/", DecoratedTokenRefreshView.as_view(), name="token_refresh"),
    path("token/verify/", DecoratedTokenVerifyView.as_view(), name="token_verify"),
]
