from django.apps import AppConfig


class TempcodConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "tempcod"
