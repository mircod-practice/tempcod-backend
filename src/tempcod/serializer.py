from rest_framework import serializers

from users.models import Device, Measurement, User


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = "__all__"
        read_only_fields = ("is_active",)

    def create(self, validated_data):
        return Device.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.charge = validated_data.get("charge", instance.charge)
        instance.is_active = True
        instance.save()
        return instance


class MeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measurement
        fields = "__all__"
        read_only_fields = ("device", "user")

    def create(self, validated_data):
        temperature = validated_data.get("temperature")
        user = validated_data.get("user")
        user.last_temperature = temperature
        user.save()
        return super(MeasurementSerializer, self).create(validated_data)
