from django.urls import path

from tempcod.views import ConnectDeviceView, MeasurementView

urlpatterns = [
    path("device/", ConnectDeviceView.as_view(), name="connect-device"),
    path("temp/", MeasurementView.as_view(), name="measurement"),
]
