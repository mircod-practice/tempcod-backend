from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from tempcod.serializer import (
    DeviceSerializer,
    MeasurementSerializer,
)
from users.models import Device


class ConnectDeviceView(generics.GenericAPIView):
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        if user.device:
            user.device.is_active = False
            user.device.save()
        try:
            device = Device.objects.get(uuid=request.data.get("uuid"))
            serializer = self.get_serializer(device, data=request.data)
        except Device.DoesNotExist:
            serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        device = serializer.save(is_active=True)
        user.device = device
        user.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class MeasurementView(generics.GenericAPIView):
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        device = serializer.save(device=request.user.device, user=request.user)
        return Response(MeasurementSerializer(device).data, status=status.HTTP_201_CREATED)
