from django.contrib import admin
from django.contrib.auth.models import Group

from users.models import User, Measurement, Device


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    fields = (
        "username",
        "first_name",
        "last_name",
        "role",
        "last_temperature",
        "device",
        "is_superuser",
        "is_staff",
        "created_at",
        "updated_at",
        "is_active",
    )
    list_display = ("username", "first_name", "last_name", "role", "is_active")
    readonly_fields = ("is_superuser", "is_staff", "created_at", "updated_at", "last_temperature")


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    fields = ("uuid", "is_active", "charge")
    list_display = ("uuid", "is_active", "charge")


@admin.register(Measurement)
class MeasurementAdmin(admin.ModelAdmin):
    fields = ("temperature", "device", "user", "measured_at")
    list_display = ("id", "temperature", "device", "measured_at")


admin.site.unregister(Group)
