from users.models import User
from django_filters import FilterSet


class UserFilter(FilterSet):
    class Meta:
        model = User
        fields = {
            "last_temperature": ["lte", "gte"],
        }
