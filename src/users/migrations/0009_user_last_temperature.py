# Generated by Django 4.0.6 on 2022-07-18 08:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0008_measurement_user"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="last_temperature",
            field=models.FloatField(blank=True, null=True),
        ),
    ]
