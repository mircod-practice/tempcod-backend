from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import gettext_lazy as _
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        if username is None:
            raise TypeError("Users must have a username.")

        user = self.model(username=username)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, username, password):
        if password is None:
            raise TypeError("Superusers must have a password.")

        user = self.create_user(username, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    MEDIC = "medic"
    PATIENT = "patient"
    ROLES = (
        (MEDIC, _("medic")),
        (PATIENT, _("patient")),
    )

    username = models.CharField(max_length=255, unique=True)
    first_name = models.CharField(max_length=255, db_index=True)
    last_name = models.CharField(max_length=255, db_index=True)
    device = models.ForeignKey("Device", on_delete=models.CASCADE, null=True, blank=True)
    last_temperature = models.FloatField(null=True, blank=True)
    role = models.CharField(max_length=10, choices=ROLES, default=PATIENT)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = "username"

    objects = UserManager()

    def __str__(self):
        return f"{self.username}"

    class Meta:
        db_table = "user"

    def measurements(self) -> list:
        return Measurement.objects.filter(user_id=self.id).order_by("-measured_at").all()


class Measurement(models.Model):
    temperature = models.FloatField()
    device = models.ForeignKey("Device", on_delete=models.CASCADE)
    measured_at = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = "measurement"


class Device(models.Model):
    uuid = models.CharField(max_length=255, unique=True, primary_key=True)
    is_active = models.BooleanField(default=False)
    charge = models.IntegerField(validators=[MaxValueValidator(100), MinValueValidator(0)])

    class Meta:
        db_table = "device"

    def __str__(self):
        return f"{self.uuid}"
