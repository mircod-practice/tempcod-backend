from rest_framework import serializers

from tempcod.serializer import MeasurementSerializer, DeviceSerializer
from users.models import User


class UserSerializer(serializers.ModelSerializer):
    device = DeviceSerializer(read_only=True, many=False)
    measurements = serializers.ListField(read_only=True, child=MeasurementSerializer())

    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name", "device", "last_temperature", "measurements", "role")
        read_only_fields = ("role", "last_temperature", "measurements", "device")


class BulkDeleteSerializer(serializers.Serializer):
    """Групповое удаление пользователей"""

    records = serializers.ListField(
        required=True, allow_empty=False, allow_null=False, child=serializers.IntegerField()
    )
