from django.urls import include, path
from rest_framework.routers import DefaultRouter

from users.views import UsersListApiView, CurrentUserView, BulkDeleteView

router = DefaultRouter()
router.register(r"user", UsersListApiView, basename="user")

urlpatterns = [
    path("", include(router.urls)),
    path("user/current", CurrentUserView.as_view()),
    path("user/bulk-delete", BulkDeleteView.as_view()),
]
