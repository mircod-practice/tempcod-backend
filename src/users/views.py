from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, mixins, generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.status import HTTP_204_NO_CONTENT
from django_filters.rest_framework import DjangoFilterBackend

from users.serializer import UserSerializer, BulkDeleteSerializer
from users.models import User
from users.filters import UserFilter


class UsersListApiView(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    serializer_class = UserSerializer
    queryset = (
        User.objects.select_related("device")
        .filter(is_active=True)
        .order_by("id")
        .exclude(is_staff=True, is_superuser=True)
    )
    permission_classes = (IsAuthenticated,)
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    search_fields = ["first_name", "last_name"]
    ordering_fields = ["username", "first_name", "last_name", "device", "last_login", "created_at"]

    filterset_class = UserFilter

    def list(self, request, *args, **kwargs):
        if request.user.role != "medic":
            raise ValidationError({"error": "Permission denied"})
        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if request.user.role != "medic":
            raise ValidationError({"error": "Permission denied"})
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response({"user": serializer.data})

    def destroy(self, request, *args, **kwargs):
        if request.user.role != "medic":
            raise ValidationError({"error": "Permission denied"})
        instance = self.get_object()
        instance.is_active = False
        instance.save()
        return Response(status=HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        if request.user.role != "medic":
            raise ValidationError({"error": "Permission denied"})

        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        if request.user.role != "medic":
            raise ValidationError({"error": "Permission denied"})

        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)


@swagger_auto_schema(operation_description="get current user data", responses={200: UserSerializer()})
class CurrentUserView(generics.GenericAPIView):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        serializer = self.get_serializer(request.user)

        return Response(serializer.data)


class BulkDeleteView(generics.GenericAPIView):
    serializer_class = BulkDeleteSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        if request.user.role != "medic":
            raise ValidationError({"error": "Permission denied"})
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        for user in User.objects.filter(id__in=serializer.data["records"]):
            if user != request.user:
                user.is_active = False
                user.save()
        return Response(serializer.data)
